<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 16:47
 * Copyright: 2012(c) Avolans.nl
 */

# CONFIG

/**
 * Timeout of the socket service, in seconds
 */
define('TIMEOUT', 30);

/**
 * IP address to listen to
 */
define('ADDRESS', '0'); #localhost
//define('ADDRESS', '87.238.173.245'); #online

/**
 * Port to listen to
 */
define('PORT', 1337);

# END CONFIG

session_start();
$_SESSION['loggedIn'] = false;

require_once('Socket.class.php');
require_once('SocketCommand.class.php');

// Include all the commands
if ($handle = opendir('commands/')){
    while (false !== ($entry = readdir($handle))){
        if ($entry == '.' OR $entry == '..'){
            continue;
        }
        require_once('commands/' . $entry);
    }
}



$s = new Socket(true);


while ($s->connected){
    $s->read();
}


