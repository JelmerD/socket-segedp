<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 16:49
 * Copyright: 2012(c) Avolans.nl
 */
class Socket
{

    /**
     * @var Object The socket variable
     */
    private $s;

    /**
     * @var Object The client variable
     */
    private $c;

    /**
     * @var Object The input
     */
    private $i;

    /**
     * @var Boolean holds the current state of the client connection
     */
    public $connected = false;

    /**
     * @var Array Keep a log of all the actions
     */
    private $log = Array();

    /**
     * Pushes the used command in this array, in order to keep cached variables of a certain command.
     * TODO make a 'reset' command that cuts the cmd from the array in order to reset the variables
     * @var array all the commands that have been used at least once
     */
    private $cachedCmds = array();

    /**
     * @var array All the available commands and functions connected to it.
     */
    private $commands = Array();

    /**
     * @param bool $start start socket
     */
    public function __construct($start = false)
    {
        if($start){
            $this->create();
            $this->bind();
            $this->listen();
            $this->accept();
            $this->displayWelcomeMessage();
        }
    }

    /**
     * Returns the log of this class to the file who calls the function
     * @return string Logfile as unordered list
     */
    public function getLog(){
        $val = "<ul>";
        foreach($this->log as $key => $value){
            $val .= "<li>[" . $key . "] " . $value . "</li>";
        }
        $val .= "</ul>";
        return $val;
    }

    /**
     * Create the socket
     */
    public function create(){
        if ($this->s = socket_create(AF_INET, SOCK_STREAM, 0)){
            $this->log[] = "Socket has been created successfully";
        } else {
            $this->log[] = "Socket could not be created";
        }
    }

    /**
     * Bind the socket to the earlier defined address and port
     */
    public function bind(){
        if (socket_bind($this->s, ADDRESS, PORT)){
            $this->log[] = "Socket has been bind successfully";
        } else {
            $this->log[] = "Socket could not bind to address '" . ADDRESS . ":" . PORT . "'";
        }
    }

    /**
     * Listen to the socket
     */
    public function listen(){
        if (socket_listen($this->s)){
            $this->log[] = "Listening to socket...";
        } else {
            $this->log[] = "Failed to listen to socket";
        }
    }

    /**
     * Accept incoming requests and handle them as child processes
     */
    public function accept(){
        if ($this->c = socket_accept($this->s)){
            $this->connected = true;
            $this->log[] = "Socket has been accepted by client";
        } else {
            $this->log[] = "Socket could not be accepted by client";
        }
    }

    /**
     * Accept incoming requests and handle them as child processes
     */
    public function read(){
        if ($this->i = trim(socket_read($this->c, 1024, PHP_NORMAL_READ))){
            if (empty($this->i)){
                return;
            }
            $this->commandHandler();
        }
    }

    public function write($msg){
        if (!socket_write($this->c, $msg . PHP_EOL)){
            $this->log[] = "Could not write to socket";
        }
    }

    /**
     * @return Object returns the last input of the socket
     */
    public function getInput(){
        return $this->i;
    }

    /**
     * close child and master sockets
     */
    public function close(){
        socket_close($this->c);
        socket_close($this->s);
        $this->connected = false;
    }

    public function commandHandler(){
        $i = explode(' ', trim($this->getInput()));
        $file = 'commands/' . $i[0] . '.php';
        if (!file_exists($file)){
            $this->write("- Command '" . $i[0] . "' does not exist. See 'cmds' for a list of all the commands");
            return false; //command does not exist
        }

        //require_once($file);
        $cmd_classname = 'cmd_' . $i[0];

        //Cache the used command in order to keep variables intact from a certain command
        if (isset($this->cachedCmds[$i[0]])){
            $cmd = $this->cachedCmds[$i[0]];
        } else {
            $cmd = new $cmd_classname();
        }

        $cmd->clearCache();

        foreach($i as $key => $value){
            if ($key == 0){
                continue;
            }
            $cmd->registerParam($i[$key]);
        }
        $cmd->run();
        $this->cachedCmds[$i[0]] = $cmd;
        return true;
    }

    private function displayWelcomeMessage(){
        $this->write(
            "Hi there and welcome to the socket program of Jelmer Droge. Curious of what is possible? Check out the 'cmds'." .
            "Just type it and hit enter, it's pretty simple huh?" . PHP_EOL .
            "Have a look around and tell me if you want to see more!" . PHP_EOL . PHP_EOL .
            "Cee ya, Jelmer" . PHP_EOL .
            "-----------------------------------------" . PHP_EOL
        );
    }

}
