<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_help extends SocketCommand
{

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "Get help for a command";
        $this->longDescription = "If you need help for a certain command, use this function. See 'cmds' for a list of all the available commands" . PHP_EOL .
            "# PARAMETERS:" . PHP_EOL .
            "<command> : The command you need help with";
    }

    public function noParameters(){
       $this->s->write("- Wrong input: help <command>");
       $this->s->write("- Note: see 'cmds' for a list of all the commands");
    }

    public function decodeParameters(){
        $cmds = new cmd_cmds();
        $cmds = $cmds->getCommandsList();
        if (!in_array($this->params[0], $cmds)){
            $this->s->write("- The command '" . $this->params[0] . "' does not exist. See 'cmds' for a list of all the commands");
            return;
        }
        $cmds = null;
        $this->getHelpMessage();
    }

    private function getHelpMessage(){
        $cmd_classname = 'cmd_' . $this->params[0];
        $cmd = new $cmd_classname();

        $this->s->write('HELP FOR: ' . strtoupper($this->params[0]));
        $this->s->write('-----------------------------------------');
        $this->s->write($cmd->getLongDescription());
        $this->s->write('-----------------------------------------');

    }

}
