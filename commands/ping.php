<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_ping extends SocketCommand
{

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "See if the socket is working properly";
        $this->longDescription = "If you wan to see if the socket is working, use the old fashioned 'ping'." . PHP_EOL .
            "# PARAMETERS: N/A";
    }

    public function noParameters(){
        $this->s->write('- pong');
    }

}
