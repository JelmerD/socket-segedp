<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_register extends SocketCommand
{

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "Register yourself for the program";
        $this->longDescription = "Want to use the functions you get when logged in but don't have an account? Register yourself! See 'help login' for more information about this process." .  PHP_EOL .
            "# PARAMETERS:" . PHP_EOL .
            "<username> <password> : The username and password you'd like to register with";
    }

    public function noParameters(){
       $this->s->write('- Wrong input: register <username> <password>');
    }

    public function decodeParameters(){
        if (count($this->params) != 2){
            $this->noParameters();
            return;
        }

        $login = new cmd_login();
        if ($login->checkUserExist($this->params[0])){
            $this->s->write('- The username \'' . $this->params[0] . '\' has already been taken');
            return;
        }

        if (file_put_contents('bin/users.txt', $this->params[0] . ' ' . sha1($this->params[1]) . "\n", FILE_APPEND)){
            $this->s->write('- You are successfully registered as \'' . $this->params[0] . '\'.');
        }

    }

}
