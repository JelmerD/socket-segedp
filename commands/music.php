<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_music extends SocketCommand
{

    /**
     * @var array The directories separated as an array value for easier use
     */
    private $directory = array('D:\Music');

    /**
     * @var array The content of the current directory (for easier access by play and open)
     */
    private $dirContent = array();

    /**
     * @var array All the allowed audio files
     */
    private $allowedAudioFiles = array(
        'mp3',
        'wav'
    );

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "Play music on the server";
        $this->longDescription = "Want to play a song on the server? Use this command. You have access to every directory childing from " . $this->directory[0] . "." .  PHP_EOL .
            "# PARAMETERS:" . PHP_EOL .
            "list : Show a list of all the directories and files in the current folder" . PHP_EOL .
            "open <directory ID> : Open a directory by typing the ID of the directory as second parameter" . PHP_EOL .
            "back : Bored of this folder? Go back to the parent directory" . PHP_EOL .
            "play <file ID> : Play an audio file by typing the ID of the file as second parameter";
        $this->updateDirContent();
    }

    public function noParameters(){
        $this->s->write("- This command needs more parameters. See 'help music' for all the available functions");
    }

    /**
     * Decode all the predefined parameters
     */
    public function decodeParameters(){
        switch ($this->params[0]){
            case 'list':
                $this->showList();
                break;
            case 'open':
                $this->open();
                break;
            case 'back':
                $this->back();
                break;
            case 'play':
                $this->play();
                break;
            default:
                $this->s->write("- This parameter is not valid. See 'help music' for more info about this command");
                break;
        }
    }

    /**
     * Show a list of the current directory you are in including all the files which are in the allowedAudioFiles array
     */
    private function showList(){
        $dir = $this->getDirAsString();
        if ($handle = opendir($dir)){
            $this->dirContent = array();
            $id = 0;
            $this->s->write('LIST DIRECTORY: ' . $dir);
            $this->s->write('-----------------------------------------');
            while (false !== ($entry = readdir($handle))) {
                if ($entry == '.' OR $entry == '..'){
                    continue;
                }
                $type = "N/A";
                if (is_dir($dir . $entry)){
                    $type = 'DIR';
                }
                if (is_file($dir . $entry)){
                    $type = "FILE";

                    if (preg_match('~\.[a-zA-Z0-9]*$~' ,$entry, $extension)){
                        $extension = substr($extension[0], '1');
                        $type = $extension;
                        if (!in_array(strtolower($type), $this->allowedAudioFiles)){
                            continue;
                        }
                    }

                }
                $this->dirContent[$id] = $entry;
                $this->s->write($id . " [" . filetype($dir . $entry) . "] " . $entry);
                $id++;
            }
            $this->s->write('-----------------------------------------');
            closedir($handle);
        }
    }

    /**
     * Open the directory with the ID of parameter 2
     * @return mixed
     */
    private function open(){
        $dir = $this->getDirAsString();
        $id = $this->params[1];

        if (!is_numeric($id)){
            $this->s->write("- Expected the second parameter to be a numeric value");
            return;
        }

        if (!isset($this->dirContent[$id])){
            $this->s->write("- ID '" . $id . "' does not exist");
            return;
        }

        if (!is_dir($dir . $this->dirContent[$id])){
            $this->s->write("- ID '" . $id . "' is not a directory, use 'music play " . $id . "' instead");
            return;
        }

        $this->directory[] = $this->dirContent[$id];
        $this->showList();
    }

    /**
     * Go back one folder (parent)
     * @return mixed
     */
    private function back(){
        if (count($this->directory) == 1){
            $this->s->write('- Can not open the parent directory');
            return;
        }
        array_pop($this->directory);
        $this->showList();
    }

    /**
     * Play an audio file with the ID of parameter 2
     * @return mixed
     */
    private function play(){
        $dir = $this->getDirAsString();
        $id = $this->params[1];
        if (!is_numeric($id)){
            $this->s->write("- Expected the second parameter to be a numeric value");
            return;
        }

        if (!isset($this->dirContent[$id])){
            $this->s->write("- ID '" . $id . "' does not exist");
            return;
        }

        if (!is_file($dir . $this->dirContent[$id])){
            $this->s->write("- ID '" . $id . "' is not a file, use 'music open " . $id . "' instead");
            return;
        }

        $file = $dir . $this->dirContent[$id];
        shell_exec('start /B "C:\Program Files\Windows Media Player\wmplayer.exe" "' . $file . '"');
        $this->s->write('- Now playing: ' . $this->dirContent[$id]);
    }

    /**
     * Pushes all the allowed directories/files in an array for use of the open and play command
     */
    private function updateDirContent(){
        $dir = $this->getDirAsString();
        if ($handle = opendir($dir)){
            $this->dirContent = array();
            $id = 0;
            while (false !== ($entry = readdir($handle))) {
                if ($entry == '.' OR $entry == '..'){
                    continue;
                }
                if (is_file($dir . $entry)){
                    if (preg_match('~\.[a-zA-Z0-9]*$~' ,$entry, $extension)){
                        if (!in_array(strtolower(substr($extension[0], '1')), $this->allowedAudioFiles)){
                            continue;
                        }
                    }
                }
                $this->dirContent[$id] = $entry;
                $id++;
            }
            closedir($handle);
        }
    }

    private function getDirAsString(){
        $dir = '';
        foreach($this->directory as $val){
            $dir .= $val . '\\';
        }
        return $dir;
    }

}
