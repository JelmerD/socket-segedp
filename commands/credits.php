<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_credits extends SocketCommand
{

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "See who built the program";
        $this->longDescription = "Curious who built the program and why? Check out this command!" . PHP_EOL .
            "# PARAMETERS: N/A";
    }

    public function noParameters(){
        $this->s->write('CREDITS:');
        $this->s->write('-----------------------------------------');
        $this->s->write('This program is built upon instructions from Hogeschool Utrecht for TCMT-SEGEDP');
        $this->s->write('* Author: Jelmer Droge');
        $this->s->write('* Date: 03-06-2012');
        $this->s->write('* Copyright: 2012(c) Avolans.nl');
        $this->s->write('-----------------------------------------');
    }
}
