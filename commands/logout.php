<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_logout extends SocketCommand
{

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "Log out of the current session";
        $this->longDescription = "Quit the current session you are in. See 'help login' for more information about this process". PHP_EOL .
            "# PARAMETERS: N/A";
    }

    public function noParameters(){
        if ($_SESSION['loggedIn'] === false){
            $this->s->write("- You were not logged in. Use 'login <username> <password>' to login.");
            return false;
        }
        $_SESSION['loggedIn'] = false;
        $this->s->write("- You are successfully logged out");
        return true;



    }

}
