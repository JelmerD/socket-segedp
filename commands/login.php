<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_login extends SocketCommand
{

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "Login with your username and password";
        $this->longDescription = "If you need permission for a certain command, you can login with your username and password by using this command." .
            "If you don't have an account, you can use register (see 'help register')" . PHP_EOL .
            "# PARAMETERS:" . PHP_EOL .
            "<username> <password> : your username and password you registered with";
    }

    public function noParameters(){
       $this->s->write('- Wrong input: login <username> <password>');
    }

    public function decodeParameters(){
        if (count($this->params) != 2){
            $this->noParameters();
            return;
        }

        $this->login($this->params[0], sha1($this->params[1]));

    }

    public function checkUserExist($user){
        $data = file_get_contents('bin/users.txt');
        $rows = explode("\n", $data);
        foreach($rows as $row){
            $rowUser = explode(' ', $row);
            if ($rowUser[0] == $user){
                return $rowUser[1];
            }
        }
        return false;
    }

    public function loggedIn(){
        if ($_SESSION['loggedIn'] !== false){
            return $_SESSION['loggedIn'];
        }
        return false;
    }

    public function setLogin($user){
        $_SESSION['loggedIn'] = $user;
    }

    public function login($username, $password){
        if ($this->loggedIn() !== false){
            $this->s->write("- You are already logged in as '" . $this->loggedIn() . "'");
            return false;
        }

        if (!$this->checkUserExist($username)){
            $this->s->write("- Username '" . $username . "' does not exist");
            return false;
        }

        if ($this->checkUserExist($username) !== $password){
            $this->s->write("- You entered a wrong password for user '" . $username . "'");
            return false;
        }

        $this->s->write("- You are now logged in as '" . $username . "', welcome!");
        $this->setLogin($username);
        return true;

    }

}
