<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_whois extends SocketCommand
{

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "Check if you are logged in";
        $this->longDescription = "If you want to check if you are logged in and as who. Use this command." . PHP_EOL .
            "# PARAMETERS: N/A";
    }

    public function noParameters(){
        if ($_SESSION['loggedIn'] === false){
            $this->s->write("- You are not logged in. Use 'login <username> <password>' to login.");
            return false;
        }
       $this->s->write("- Logged in as: '" . $_SESSION['loggedIn'] . "'");
       return true;
    }

}
