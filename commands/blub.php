<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_blub extends SocketCommand
{

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "Returns all the available commands in this socket program";
        $this->longDescription = "This command allows you to see every single command that is available in this program" . PHP_EOL .
            "# PARAMETERS: N/A";
    }

    public function noParameters(){
        $this->s->write('AVAILABLE COMMANDS:');
        $this->s->write('-----------------------------------------');
        if ($handle = opendir('commands/')){
            while (false !== ($entry = readdir($handle))){
                if ($entry == '.' OR $entry == '..'){
                    continue;
                }

                $cmd_name = substr($entry, 0, -4);
                $cmd_classname = 'cmd_' . $cmd_name;
                $cmd = new $cmd_classname();
                $this->s->write($cmd_name . "\t\t" . $cmd->getShortDescription());
            }
        }
        $this->s->write('-----------------------------------------');
    }

    /**
     * Get all the available commands in this socket server
     * @return array The available commands as plain text per index
     */
    public function getCommandsList(){
        $cmds = array();
        if ($handle = opendir('commands/')){
            while (false !== ($entry = readdir($handle))){
                if ($entry == '.' OR $entry == '..'){
                    continue;
                }
                $cmds[] = substr($entry, 0, -4);
            }
        }
        return $cmds;
    }

}
