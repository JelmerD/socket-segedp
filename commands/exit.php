<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:07
 * Copyright: 2012(c) Avolans.nl
 */
class cmd_exit extends SocketCommand
{

    public function __construct()
    {
        parent::__construct();
        $this->shortDescription = "Exits the program and closes the socket";
        $this->longDescription = "If you want to exit the program but are to lazy to close everything manually, use this" . PHP_EOL .
            "# PARAMETERS: N/A";
    }

    public function noParameters(){
        $this->s->close();
    }

}
