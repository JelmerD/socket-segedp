<?php
/**
 * Author: Jelmer Dröge
 * Date: 3-6-12
 * Time: 20:13
 * Copyright: 2012(c) Avolans.nl
 */
class SocketCommand
{

    /**
     * @var array All the parameters that has been typed after the command
     */
    protected $params = array();

    /**
     * @var Socket The socket, it had to be imported to the class
     */
    protected $s;

    /**
     * @var string A short description of the command (used by cmds)
     */
    protected $shortDescription = "This command has no short description yet";

    /**
     * @var string A long description of the command (used by help)
     */
    protected $longDescription = "Sorry, there is no help for this command available :(";

    public function __construct()
    {
        global $s;
        $this->s = $s;
    }

    public function clearCache(){
        $this->params = array();
    }

    public function registerParam($param){
        $this->params[] = $param;
    }

    /**
     * Run the command including the parameters
     */
    public function run(){
        if (count($this->params) == 0){
            $this->noParameters();
        } else {
            $this->decodeParameters();
        }
    }

    public function noParameters(){
        $this->s->write("The command exists, but there is no function for it yet.");
    }

    public function decodeParameters(){
        $this->s->write("This command does not support parameters");
    }

    public function getShortDescription(){
        return $this->shortDescription;
    }

    public function getLongDescription(){
        return $this->longDescription;
    }

}
